/* 
    TASK 1

    Дан объект : 

    const car = {
        company : 'Toyota',
        model : 'Land Cruser',
        doors : 5,
        color : 'white'
    }

    Необходимо преобразовать данный объект в формат JSON , и потом обратно.
    Запишите оба результата в переменную и выведите их значения в консоль.

*/

const car = {
    company: 'Toyota',
    model: 'Land Cruser',
    doors: 5,
    color: 'white'
}
const carStr = JSON.stringify(car);
console.log(carStr);
const carObj = JSON.parse(carStr);
console.log(carObj);

/* 
    TASK 2

    Воспользуйтесь free REST API: https://jsonplaceholder.typicode.com/ для получения 
    100 albums. И выведите все альбомы на html страницу в виде : 

    UserId : значение userId с пришедшего вам объекта,
    Id : значение Id с пришедшего вам объекта,
    Title : значение title с пришедшего вам объекта

    В итоге на вашей странице должно распарситься 100 разных альбомов. 

*/

const xhr = new XMLHttpRequest();
const url = 'https://jsonplaceholder.typicode.com/albums'
var objects = [];
xhr.open('Get', url);
xhr.addEventListener('load', () => {
    console.log(JSON.parse(xhr.response));
    objects = JSON.parse(xhr.response);
    successRequired(objects);
})
xhr.send();
const successRequired = objects => {
    for (let i = 0; i < objects.length; i++) {
        var header1 = '<h1> ' + " UserId :" + objects[i].userId + '</h1>';
        var header2 = '<h2> ' + " Id :" + objects[i].id + '</h2>';
        var header3 = '<h3> ' + " Title :" + objects[i].title + '</h3>';
        document.getElementById('div').innerHTML += header1;
        document.getElementById('div').innerHTML += header2;
        document.getElementById('div').innerHTML += header3;
    }
}









